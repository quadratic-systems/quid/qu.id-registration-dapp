import quadratic from './quadratic.js'
import quid from './quid.js'
import quidregistrar from './quidregistrar.js'

export {
  quadratic,
  quid,
  quidregistrar
}