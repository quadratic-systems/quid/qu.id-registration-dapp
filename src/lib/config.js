import { sepolia, anvil } from "viem/chains";
import * as abi from "./abi/index.js";

export default {
  supportedChains: [sepolia, anvil],
  projectId: "c848fbbc85b4929d2b0d5125ccf4575b",
  metadata: {
    name: "Qu.Id Registration Dapp",
    description: "Register a quadratic identifier with the registrar.",
    url: "https://registrar.quadratic.systems", // origin must match your domain & subdomain
    icons: ["https://avatars.githubusercontent.com/u/37784886"],
  },
  abi,
  chain: {
    31337: {
      quidregistrar: {
        address: "0x0165878A594ca255338adfa4d48449f69242Eb8F",
      },
    },
    11155111: {
      quidregistrar: {
        address: "0xE0B021272b4C2688B210a0A13D263d2af843290F",
      },
    },
  },
};
