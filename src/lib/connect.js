import { createWeb3Modal, defaultWagmiConfig } from "@web3modal/wagmi";
import { reconnect, getAccount, watchAccount } from "@wagmi/core";
import appConfig from "./config.js";

console.log("registrar app config", appConfig);

// Create wagmiConfig
const config = defaultWagmiConfig({
  chains: appConfig.supportedChains,
  projectId: appConfig.projectId,
  metadata: appConfig.metadata,
});

let account;
let unwatch;

window.addEventListener("load", async () => {
  reconnect(config);

  createWeb3Modal({
    wagmiConfig: config,
    projectId: appConfig.projectId,
    themeMode: "light",
  });

  unwatch = watchAccount(config, {
    onChange(data) {
      account = getAccount(config);

      const onwalletconnected = new CustomEvent("onwalletconnected", {
        detail: { account, config },
      });

      const onwalletdisconnected = new CustomEvent("onwalletdisconnected", {
        detail: { account, config },
      });
      if (data.isConnected && account.chain) {
        // if connected  but still "connecting", do nothing.
        if (data.status === "connected") {
          document.dispatchEvent(onwalletconnected);
        }
      } else {
        document.dispatchEvent(onwalletdisconnected);
      }
    },
  });
});

window.addEventListener("unload", () => {
  unwatch();
});
