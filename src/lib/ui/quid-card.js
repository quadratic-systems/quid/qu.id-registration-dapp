export default (details) => {
  return `<h3 class="card-title">${details.handle}</h3>
      <div class="card-body">
        <p>${details.uri}</p>
      </div>
      <div class="card-footer">
        <div class="quid-actions">
          <button class="change-uri" data-id="${details.id}" data-uri="${details.uri}" data-handle="${details.handle}">
            <svg xmlns="http://www.w3.org/2000/svg" width="1.5rem" height="1.5rem" viewBox="0 0 24 24"><g fill="none" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path d="M7 7H6a2 2 0 0 0-2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2-2v-1"/><path d="M20.385 6.585a2.1 2.1 0 0 0-2.97-2.97L9 12v3h3zM16 5l3 3"/></g></svg>
          </button>
          <button class="deregister" data-id="${details.id}" data-uri="${details.uri}" data-handle="${details.handle}">
            <svg xmlns="http://www.w3.org/2000/svg" width="1.5rem" height="1.5rem" viewBox="0 0 24 24"><path fill="black" d="M7 21q-.825 0-1.412-.587T5 19V6H4V4h5V3h6v1h5v2h-1v13q0 .825-.587 1.413T17 21zM17 6H7v13h10zM9 17h2V8H9zm4 0h2V8h-2zM7 6v13z"/></svg>
          </button>
          </div>
          <div>Staked:&nbsp;<span class="badge">${details.staked} QUAD</span></div>
      </div>`;
};
