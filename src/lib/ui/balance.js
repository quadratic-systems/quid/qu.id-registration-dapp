import getQuadBalance from "../rpc/get-quadratic-balance.js";
import getMinimumRegisterStake from "../rpc/get-minimum-register-stake.js";

export default async (DOM) => {
  let account, config;
  let interval;
  let current;
  let minimumStakeAmount;

  document.addEventListener("onwalletconnected", async (event) => {
    ({ account, config } = event.detail);
    minimumStakeAmount = await getMinimumRegisterStake({ config });

    update();
    startPolling();

    DOM.quadBalanceDetails.classList.add("show");
    DOM.minimumStakeAmount.innerText = minimumStakeAmount;
  });

  document.addEventListener("onwalletdisconnected", async () => {
    DOM.quadBalanceDetails.classList.remove("show");
    DOM.quadBalance.innerText = 0;
    stopPolling();
  });

  const check = async () => {
    stopPolling();
    update();
    startPolling();
  };

  const update = async () => {
    current = await getQuadBalance(account.addresses[0], {
      config,
    });

    DOM.quadBalance.innerText = current;

    if (current < minimumStakeAmount) {
      DOM.insufficientQuadBalance.style.display = "block";
      DOM.register.style.display = "none";
    } else {
      DOM.register.style.display = "block";
      DOM.insufficientQuadBalance.style.display = "none";
    }
  };

  const startPolling = () => {
    interval = setInterval(async () => {
      await update();

      const oldBalance = parseInt(DOM.quadBalance.innerText);
      const newBalance = current;

      if (oldBalance !== newBalance) {
        DOM.quadBalance.innerText = newBalance;
      }
    }, 30000);
  };

  const stopPolling = () => {
    clearInterval(interval);
  };

  return {
    check,
  };
};
