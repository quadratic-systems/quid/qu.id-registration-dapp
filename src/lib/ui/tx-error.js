export default (parent, receipt) => {
  const receiptJson = JSON.stringify(receipt, (key, value) =>
    typeof value === "bigint" ? Number(value) : value,
  );

  parent.innerHTML = `<div class="tx-error-grid">
        <h4>An error has occurred.</h4>
        <p>If the problem persists, please copy the following error and submit it for further assistance.</p>
        <textarea id="txError" readonly>${receiptJson}</textarea>
        <button id="txErrorCopy">Copy</button>
    </div>`;

  document.getElementById("txErrorCopy").addEventListener("click", (event) => {
    event.preventDefault();
    document.getElementById("txError").select();

    // Copy the text inside the text field
    navigator.clipboard.writeText(document.getElementById("txError").value);

    document.getElementById("txErrorCopy").textContent = "Copied!";

    setTimeout(() => {
      document.getElementById("txErrorCopy").textContent = "Copy";
    }, 2000);
  });
};
