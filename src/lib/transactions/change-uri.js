import { simulateContract, writeContract, getChainId } from "@wagmi/core";
import appConfig from "../config.js";

export default async (id, uri, { config }) => {
  const { abi, chain } = appConfig;
  const chainId = await getChainId(config);

  const { request } = await simulateContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "changeURI",
    args: [id, uri],
  });

  const hash = await writeContract(config, request);

  return hash;
};
