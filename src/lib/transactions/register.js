import { parseSignature } from "viem";
import { simulateContract, writeContract, getChainId } from "@wagmi/core";
import appConfig from "../config.js";

export default async (uri, deadline, value, signature, { config }) => {
  const { abi, chain } = appConfig;
  const chainId = await getChainId(config);

  const { r, s, v } = parseSignature(signature);

  const { request } = await simulateContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "register",
    args: [uri, value, deadline, v, r, s],
  });

  const hash = await writeContract(config, request);

  return hash;
};
