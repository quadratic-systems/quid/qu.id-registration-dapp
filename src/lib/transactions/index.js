import sign from "./sign.js";
import register from "./register.js";
import changeUri from "./change-uri.js";
import approve from "./approve.js";
import deregister from "./deregister.js";

export { sign, register, changeUri, approve, deregister };
