import { readContract, getToken, getChainId, signTypedData } from "@wagmi/core";
import getQuadraticAddress from "../rpc/get-quadratic-address.js";
import appConfig from "../config.js";

export default async (uri, { account, config }) => {
  const { abi, chain } = appConfig;
  const deadline = Math.floor(Date.now() / 1000) + 4200;
  const chainId = await getChainId(config);
  const quadratic = await getToken(config, {
    address: await getQuadraticAddress({ config }),
  });

  const value = await readContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "minimumStakeAmount",
    args: [],
  });

  const nonce = await readContract(config, {
    abi: abi.quadratic,
    address: quadratic.address,
    functionName: "nonces",
    args: [account],
  });

  const domain = {
    name: quadratic.name,
    version: "1",
    chainId,
    verifyingContract: quadratic.address,
  };

  const types = {
    Permit: [
      {
        name: "owner",
        type: "address",
      },
      {
        name: "spender",
        type: "address",
      },
      {
        name: "value",
        type: "uint256",
      },
      {
        name: "nonce",
        type: "uint256",
      },
      {
        name: "deadline",
        type: "uint256",
      },
    ],
  };

  // set the Permit type values
  const message = {
    owner: account,
    spender: chain[chainId].quidregistrar.address,
    value,
    nonce,
    deadline,
  };

  const signature = await signTypedData(config, {
    domain,
    types,
    message,
    primaryType: "Permit",
  });

  return { signature, deadline, value };
};
