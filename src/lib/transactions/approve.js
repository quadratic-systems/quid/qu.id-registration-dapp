import { simulateContract, writeContract, getChainId } from "@wagmi/core";
import getQuidAddress from "../rpc/get-quid-address.js";
import appConfig from "../config.js";

export default async (id, { config }) => {
  const { abi, chain } = appConfig;
  const chainId = await getChainId(config);

  const quid = await getQuidAddress({ config });

  const { request } = await simulateContract(config, {
    abi: abi.quid,
    address: quid,
    functionName: "approve",
    args: [chain[chainId].quidregistrar.address, id],
  });

  const hash = await writeContract(config, request);

  return hash;
};
