import { readContract, getChainId } from "@wagmi/core";
import { formatEther } from "viem";
import getQuidAddress from "./get-quid-address.js";
import appConfig from "../config.js";

export default async (id, { config }) => {
  const { abi, chain } = appConfig;
  const chainId = await getChainId(config);

  const quid = await getQuidAddress({ config });

  const handle = await readContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "getHandle",
    args: [id],
  });

  const tokenUri = await readContract(config, {
    abi: abi.quid,
    address: quid,
    functionName: "tokenURI",
    args: [id],
  });

  const staked = await readContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "staked",
    args: [id],
  });

  return { id, handle, uri: tokenUri, staked: formatEther(staked) };
};
