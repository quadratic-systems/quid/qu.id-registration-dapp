import { readContract, getChainId } from "@wagmi/core";
import { formatEther } from "viem";
import appConfig from "../config.js";

export default async ({ config }) => {
  const { abi, chain } = appConfig;
  const chainId = await getChainId(config);

  const minimumStakeAmount = await readContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "minimumStakeAmount",
    args: [],
  });

  return formatEther(minimumStakeAmount);
};
