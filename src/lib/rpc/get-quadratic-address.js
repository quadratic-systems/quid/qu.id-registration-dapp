import { readContract, getChainId } from "@wagmi/core";
import appConfig from "../config.js";

export default async ({ config }) => {
  const { abi, chain } = appConfig;
  const chainId = await getChainId(config);
  return await readContract(config, {
    abi: abi.quidregistrar,
    address: chain[chainId].quidregistrar.address,
    functionName: "quadratic",
    args: [],
  });
};
