import { readContract } from "@wagmi/core";
import appConfig from "../config.js";
import getQuidAddress from "./get-quid-address.js";
import getQuId from "./get-quid.js";

const getTokensByOwner = async (account, { config }) => {
  const { abi } = appConfig;

  const quid = await getQuidAddress({ config });

  const balance = await readContract(config, {
    abi: abi.quid,
    address: quid,
    functionName: "balanceOf",
    args: [account],
  });

  const tokens = [];

  for (var i = 0; i < balance; i++) {
    const tokenId = await readContract(config, {
      abi: abi.quid,
      address: quid,
      functionName: "tokenOfOwnerByIndex",
      args: [account, i],
    });

    tokens[i] = await getQuId(tokenId, { config });
  }

  return tokens;
};

export default getTokensByOwner;
