import { readContract } from "@wagmi/core";
import { formatEther } from "viem";
import getQuadraticAddress from "./get-quadratic-address.js";
import appConfig from "../config.js";

export default async (address, { config }) => {
  const { abi } = appConfig;

  const quad = await getQuadraticAddress({ config });

  const balance = await readContract(config, {
    abi: abi.quadratic,
    address: quad,
    functionName: "balanceOf",
    args: [address],
  });

  return formatEther(balance);
};
