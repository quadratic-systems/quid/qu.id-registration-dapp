import { readContract, getChainId } from "@wagmi/core";
import getQuidAddress from "./get-quid-address.js";
import appConfig from "../config.js";

export default async (id, { config }) => {
  const { chain, abi } = appConfig;
  const chainId = await getChainId(config);

  const quid = await getQuidAddress({ config });

  const address = await readContract(config, {
    abi: abi.quid,
    address: quid,
    functionName: "getApproved",
    args: [id],
  });

  return address === chain[chainId].quidregistrar.address;
};
