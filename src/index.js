import { waitForTransactionReceipt } from "@wagmi/core";
import {
  sign,
  register,
  changeUri,
  approve,
  deregister,
} from "./lib/transactions/index.js";
import { getTokensByOwner, isQuIdApproved } from "./lib/rpc/index.js";
import { DOM, getBalance, quidCard, loading, txError } from "./lib/ui/index.js";
import { version } from "../package.json";
import "./lib/connect.js";

let account, config, balance;

getBalance(DOM).then((value) => {
  balance = value;
});

let tx;

const handleChangeUri = (event) => {
  const id = event.currentTarget.getAttribute("data-id");
  const uri = event.currentTarget.getAttribute("data-uri");
  const handle = event.currentTarget.getAttribute("data-handle");
  DOM.changeUriDialog.querySelector("#handle").innerText = handle;
  DOM.changeUriForm.elements.id.value = id;
  DOM.changeUriForm.elements.uri.value = uri;
  DOM.changeUriDialog.showModal();
};

const handleDeregister = async (event) => {
  const id = event.currentTarget.getAttribute("data-id");
  const handle = event.currentTarget.getAttribute("data-handle");

  DOM.deregisterDialog.querySelector("#handle").innerText = handle;
  DOM.deregisterForm.elements.id.value = id;

  const isApproved = await isQuIdApproved(id, {
    config,
  });

  if (isApproved) {
    DOM.deregisterForm.elements.deregister.disabled = false;
    DOM.deregisterForm.elements.approveDeregister.classList.remove("show");
  }

  DOM.deregisterDialog.showModal();
};

const renderTokensByOwner = async (account, { config }) => {
  const tokens = (await getTokensByOwner(account, { config })).reverse();

  DOM.quids.replaceChildren();

  Object.entries(tokens).forEach(([, value]) => {
    const card = document.createElement("div");
    card.classList.add("card");
    card.innerHTML = quidCard(value);
    DOM.quids.appendChild(card);
  });

  document
    .querySelectorAll(".change-uri")
    .forEach((el) => el.addEventListener("click", handleChangeUri));

  document
    .querySelectorAll(".deregister")
    .forEach((el) => el.addEventListener("click", handleDeregister));
};

DOM.registerUri.addEventListener("input", (event) => {
  if (event.target.validity.valid) {
    DOM.approveRegister.classList.add("show");
    DOM.registerError.textContent = "";
  } else {
    DOM.approveRegister.classList.remove("show");
  }
});

DOM.approveRegisterInfo.addEventListener("click", (event) => {
  event.preventDefault();
  DOM.approveRegisterInfoPopup.classList.toggle("show");
});

DOM.approveDeregisterInfo.addEventListener("click", (event) => {
  event.preventDefault();
  DOM.approveDeregisterInfoPopup.classList.toggle("show");
});

DOM.done.addEventListener("click", () => {
  event.preventDefault();
  DOM.doneDialog.close();
});

DOM.registerForm.addEventListener("submit", async (event) => {
  event.preventDefault();

  const uri = DOM.registerForm.elements.uri;

  if (uri.validity.valid) {
    DOM.registerError.textContent = "";

    if (event.submitter.id === "register") {
      const { signature, deadline, value } = tx;
      const hash = await register(uri.value, deadline, value, signature, {
        config,
      });

      const label = DOM.register.innerHTML;
      DOM.register.disabled = true;
      DOM.register.innerHTML = loading;

      const transactionReceipt = await waitForTransactionReceipt(config, {
        hash,
      });

      DOM.register.innerHTML = label;

      console.log("register", "receipt", transactionReceipt);

      if (transactionReceipt.status === "success") {
        uri.value = "";
        await renderTokensByOwner(account.addresses[0], { config });
        await balance.check();
      } else {
        txError(document.getElementById("registerTxError"), transactionReceipt);
        DOM.register.disabled = false;
      }
    } else {
      tx = await sign(uri.value, { account: account.addresses[0], config });

      if (tx) {
        DOM.register.disabled = false;
        DOM.approveRegister.classList.remove("show");
      }
    }
  } else {
    DOM.registerError.textContent = "Please enter a valid url.";
  }
});

DOM.changeUriForm.addEventListener("submit", async (event) => {
  event.preventDefault();

  const id = DOM.changeUriForm.elements.id.value;
  const uri = DOM.changeUriForm.elements.uri;

  if (uri.validity.valid) {
    DOM.changeUriError.textContent = "";

    const hash = await changeUri(id, uri.value, { config });

    const label = DOM.changeUri.innerHTML;
    DOM.changeUri.disabled = true;
    DOM.changeUri.innerHTML = loading;

    const transactionReceipt = await waitForTransactionReceipt(config, {
      hash,
    });

    DOM.changeUri.innerHTML = label;

    console.log("change uri", "receipt", transactionReceipt);

    if (transactionReceipt.status === "success") {
      uri.value = "";
      DOM.changeUriDialog.close();
      DOM.doneDialog.showModal();
    } else {
      txError(document.getElementById("changeUriTxError"), transactionReceipt);
    }

    await renderTokensByOwner(account.addresses[0], { config });
  } else {
    DOM.changeUriError.textContent = "Please enter a valid url.";
  }
});

DOM.deregisterForm.addEventListener("submit", async (event) => {
  event.preventDefault();

  const id = DOM.deregisterForm.elements.id.value;

  let hash;

  let button;
  if (event.submitter.id === "deregister") {
    hash = await deregister(id, { config });
    button = DOM.deregister;
  } else if (event.submitter.id === "approveDeregister") {
    hash = await approve(id, { config });
    button = DOM.approveDeregister;
  }

  const label = button.innerHTML;
  button.disabled = true;
  button.innerHTML = loading;

  const transactionReceipt = await waitForTransactionReceipt(config, {
    hash,
  });

  button.innerHTML = label;

  console.log("deregister", "receipt", transactionReceipt);

  if (transactionReceipt.status === "success") {
    if (event.submitter.id === "deregister") {
      await renderTokensByOwner(account.addresses[0], { config });
      DOM.deregisterDialog.close();
      DOM.doneDialog.showModal();
    } else {
      DOM.deregister.disabled = false;
      DOM.approveDeregister.classList.remove("show");
    }
  } else {
    txError(document.getElementById("deregisterTxError"), transactionReceipt);
    button.disabled = false;
  }
});

DOM.modalClose.forEach((el) =>
  el.addEventListener("click", () => {
    document.getElementById(el.getAttribute("data-parent")).close();
  }),
);

document.addEventListener("onwalletconnected", async (event) => {
  ({ account, config } = event.detail);

  DOM.connectWallet.style.display = "none";

  DOM.registerUri.disabled = false;

  await renderTokensByOwner(account.addresses[0], { config });
});

document.addEventListener("onwalletdisconnected", async () => {
  // if the wallet isn't connected, put the whole UI into an inactive state.
  DOM.connectWallet.style.display = "block";
  DOM.approveRegister.classList.remove("show");
  DOM.register.style.display = "none";
  DOM.insufficientQuadBalance.style.display = "none";
  DOM.registerUri.disabled = true;
  DOM.dialogs.forEach((el) => el.close());
  DOM.quids.innerHTML = "";
  account = null;
  config = null;
});

document.addEventListener("DOMContentLoaded", () => {
  DOM.registerUri.disabled = true;
  DOM.version.innerText = version;
});
