# registrar.quadratic.systems: The Qu.Id Resolver Registration App

registrar.quadratic.systems is a decentralized application for registering a Quadratic Identifier (Qu.Id) with the Qu.Id Registrar.

## Development

registrar.quadratic.systems can be launched in development mode by running:

```
npm run dev
```

Any updates to the code base will be reloaded in the deployed web app.

In most cases, you will access the app via http://localhost:5173 unless otherwise specified by Vite.

Once running, use Metamask or another compatible wallet to connect to the application. 

**NOTE** Make sure you connect to the correct network before interacting with the webapp.

**NOTE** The wallet you are connecting with must hold enough Quadratic tokens to meet the minimum stake requirement otherwise you will not be able to register a handle.

### Connecting to a local dev chain

If you wish to interact with a local dev version of the smart contracts, you will first need to run Anvil:

```
anvil
```

and then deploy the Quadratic Token, Quadratic Identifier token and Qu.Id Registrar to it:

```
git clone https://gitlab.com/quadratic-systems/qu.id-registrar.git
git submodule update --init --recursive
make deploy-dev
```

Deploying to dev will deploy all three contracts; Quadratic, QuId and QuIdRegistrar.

To complete the setup, configure the registrar app to use local versions of the QuIdRegistrar contract:

```
import { sepolia, anvil } from "viem/chains";
import * as abi from "./abi/index.js";

export default {
  ...
  chain: {
    31337: {
      quidregistrar: {
        address: "0x0123",
      },
    },
    ...
  },
};
```

Chain 31337 will be Anvil's default chain id. Change this if you run Anvil on a different id.

Address will be the address of the QuIdRegistrar deployed to Anvil. When running `make deploy-dev`, the address of all contracts will be printed to the terminal upon successful deployment.  

## Deployment

The Registrar app is available in a decentralized version via IPFS. However, due to limitations on Fleek (where the IPFS version of Registrar is deployed), an alternative version of the app is available for those not running a local IPFS node.

## Fleek

You will need access to the Quadratic project to be able to complete the following steps.

Start by building the Registrar app:

```
npm run build
```

This will create a distributable copy of the app in a directory called ./dist.

To deploy an IPFS version of the Registrar:

- Log in to [Fleek](https://app.fleek.xyz/),
- Click on "Store your files",
- Click "Add new..." then click "Folder upload",
- Select the "dist" directory (browse to your local copy of the registrar.quadratic.systems repo to find a copy),
- Once uploaded, it will be listed under "Files". Click the "hotdog" menu (located at the far right of the entry) and select "Edit name",
- Change the name of the directory entry to "registrar.quadratic.systems.<version>" where <version> is the semantic name of the version you have uploaded. This helps differentiate between different releases.

Once deployed, you will need to update the DNS entry.

- In Fleek, under "Files", click the "hotdog" next to your newly uploaded directory and select "Copy IPFS hash",
- In the DNS entry for the IPFS version of Registrar, update the value of the TXT record "_dnslink.registrar" to "dnslink=/ipfs/<paste-hash-here>" and replace <paste-hash-here> with the copied IPFS hash from the previous step.

## Gitlab Pages

There exists a Gitlab pipeline for deployment of a remote copy of the Registrar app. To run the pipeline, simply "tag" the repo with the same semantic version as this project's package.json file.

For example, if the new tag is v1.1.3:

```
cd /path/to/local/registrar.quadratic.systems
git tag v1.1.3
git push --tags
```

This will launch the pipeline and deploy the tagged version to Gitlab pages.